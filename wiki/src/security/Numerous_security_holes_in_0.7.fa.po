# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-19 06:45+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/numerous_"
"security_holes_in_07/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Thu Apr 28 11:12:13 2011\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 0.7\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid ""
"The following security holes affect Tails 0.7. Some of these allow **remote "
"arbitrary code execution with full administrator rights**, that can be "
"exploited by any attacker with access to the local network."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** urge you to [[upgrade to Tails 0.7.1|news/version_0.7.1]] as "
"soon as possible in case you are still using an older version."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Details\n"
msgstr "جزییات\n"

#. type: Plain text
msgid ""
"These are Debian security announces; details can be found on the [Debian "
"security page](http://security.debian.org/):"
msgstr ""

#. type: Bullet: '  - '
msgid "x11-xserver-utils (DSA-2213-1)"
msgstr ""

#. type: Bullet: '  - '
msgid "isc-dhcp (DSA-2216-1)"
msgstr ""

#. type: Bullet: '  - '
msgid "libmodplug (DSA-2226-1)"
msgstr ""

#. type: Bullet: '  - '
msgid "openjdk-6 (DSA-2224-1)"
msgstr ""

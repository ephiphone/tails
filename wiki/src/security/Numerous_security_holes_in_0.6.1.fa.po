# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-16 15:25+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/numerous_"
"security_holes_in_061/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Mon Jan 17 11:12:13 2011\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in T(A)ILS 0.6.1\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid ""
"The following security holes affect T(A)ILS 0.6.1; one of those is a bug "
"that probably allows remote code execution; see the [Tor project blog "
"post](https://blog.torproject.org/blog/tor-02129-released-security-patches)  "
"for details."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** urge you to [[upgrade to T(A)ILS 0.6.2|news/version_0.6.2]] "
"in case you are still using an older version."
msgstr ""

#. type: Bullet: '  - '
msgid "Tor (DSA-2148-1)"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Linux kernel ([many "
"issues](http://lists.debian.org/debian-testing-security-announce/2011/01/msg00011.html))"
msgstr ""

#. type: Bullet: '  - '
msgid "Iceweasel libraries: NSS (DSA-2141-2)"
msgstr ""

#. type: Bullet: '  - '
msgid "OpenSSL (DSA-2141)"
msgstr ""

#. type: Bullet: '  - '
msgid "Glibc (DSA-2122)"
msgstr ""

#. type: Bullet: '  - '
msgid "libxml2 (DSA-2137)"
msgstr ""

#. type: Bullet: '  - '
msgid "dpkg (DSA-2142)"
msgstr ""

#. type: Plain text
msgid ""
"These are Debian security announces; details can be found on "
"<http://security.debian.org/>."
msgstr ""
